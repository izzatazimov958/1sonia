import Header from "../src/components/Header";
import Reviews from "../src/components/Reviews-section/Reviews";
import Download from "../src/components/download-section/Download";
import HowItWorks from "../src/components/how it works/HowItWorks";
import Number_Facts from "../src/components/Number&Facts/Number_Facts";
import MyTello from "../src/components/mytello/MyTello";
import Countries from "../src/components/countries/Countries";
import CheapCalls from "../src/components/cheap calls/CheapCalls";
import About from "../src/components/about us/About";
import Faqs from "../src/components/Faqs/Faqs";
import Support from "../src/components/support/Support";
import Footer from "../src/components/footer/Footer";
import FooterTop from "../src/components/footer/FooterTop";

import Head from "next/head";

export default function Home() {
  return (
    <div lang="uz">
      <Head>
        <title>1SONIA</title>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE-edge" />
        <meta name="description" content="1 SONIA xizmatlari, meditsina, restoran, avto" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css"
          integrity="sha512-10/jx2EXwxxWqCLX/hHth/vu2KY3jCF70dCQB8TSgNjbCVAC/8vai53GfMDrO2Emgwccf2pJqxct9ehpzG+MTw=="
          crossorigin="anonymous" //eslint-disable-line
          referrerpolicy="no-referrer"
        />
      </Head>
      <Header />
      {/* <Reviews/> */}
      {/* <Download /> */}
      <HowItWorks />
      {/* <Number_Facts /> */}
      <MyTello />
      {/* <Countries /> */}
      {/* <CheapCalls /> */}
      <About />
      {/* <Faqs /> */}
      <Support />
      <FooterTop />
      <Footer />
    </div>
  );
}
