// import Image from "next/image";
import Image from "next/dist/client/image"

import style from "../../styles/header-content__right.module.scss";

function Header() {

  const iphone_image = require('../../../pages/images/header-section/iphone.png')
  const fakeIphone_image = require('../../../pages/images/main-section/iphone-icon.svg')

  return <div className={style.right}>
    <div className={style.image}>
      <Image src={fakeIphone_image} alt="image" />
    </div>
  </div>;
}

export default Header;
