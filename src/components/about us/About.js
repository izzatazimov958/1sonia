import Image from "next/image";
import style from "./about.module.scss";

function About() {
  const pageSub = `Biz insonlar rejalashtirgan ishlarimizni o'z vaqtida qilishni hoxlaymiz. 
  Qancha yumushimizni erta yakunlasak  va bizning bisotimizda yana  ham qo'shimcha bo'sh vaqtimiz bo'lishini bilish hissiyotiga ega bo'lishimiz qanchalar yoqimli!😊`;
  const pageTitle = "BIZ HAQIMIZDA…";

  const img = require("../../../pages/images/main-section/about-icon.svg");
  const imgText =
    "1 SONIA Jamiyatda  uchratadigan va ko'p vaqt sarflanadigan mavjud tarmoqli muammolarga intelektual elektronik yechim beradi.";

  return (
    <section className={style.about}>
      <div className="container">
        <div className={style.content}>
          <div className={style.texts}>
            <h1 className={style.title}>{pageTitle} </h1>
            <p className={style.subtitle}>{pageSub} </p>
          </div>
          <div className={style.wrapper}>
            <div className={style.imgWrapper}>
              <Image src={img} alt="image" className={style.aboutImg} />
            </div>
            <p>{imgText}</p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default About;
