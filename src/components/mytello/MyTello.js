
import Image from 'next/image'
import style from './mytello.module.scss'

function MyTello() {

  const btn = 'Start your free call now'
  const pageTitle = 'Xo’sh, nima uchun aynan  1 SONIA?!'
  const cards = [
    {
      cardAvatar: require('../../../pages/images/main-section/myTello-avatar.svg'),
      title: 'Meditsina',
      sub: `Bo'sh vaqtingizda o'zingizga yaqin bo'lgan shifokor xizmatidan foydalaning. Biz siz uchun qayg'uramiz…!`,
    },
    {
      cardAvatar: require('../../../pages/images/main-section/myTello-avatar.svg'),
      title: 'Restoran',
      sub: `Barcha xohlagan va kutgan ushbu kategoriyamizda atrofimizdagi diqqatga sazovor bo'lgan restoranlar va ularning dasturxonga tortadigan shirin taomlar ro'yxatini qamrab olingan.`,
    },
    {
      cardAvatar: require('../../../pages/images/main-section/myTello-avatar.svg'),
      title: 'Avto',
      sub: `Moshinangiz ehtiyot qismi bilan muammo bormi…? Bizda yechim bor. Atrofingizdagi tajribali ustalar xizmatingizga tayyor.`,
    },
 
  ]

  return (
    <section className={style.mytello}>
      <div className="container">
        <div className={style.content}>
          <div className={style.texts}>
            <h1 className={style.title}>{pageTitle} </h1>
          </div>
          <div className={style.cards}>
            {cards.map((card, index) => {
              return (
                <div className={style.card} key={index}>
                  <div className={style.image__box}>
                    <Image src={card.cardAvatar} alt='image' />
                  </div>
                  <h3 className={style.cardTitle}>{card.title} </h3>
                  <p className={style.cardSubtitle}>{card.sub}</p>
                </div>
              )
            })}
          </div>
          <button className={style.btn}>{btn}</button>
        </div>
      </div>
    </section>
  )
}

export default MyTello